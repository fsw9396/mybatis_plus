package mybatis_plus;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sxt.domain.User;
import com.sxt.mapper.UserMapper;

public class TestUser {
	public static void main(String[] args) {
	 ApplicationContext context = new ClassPathXmlApplicationContext("/applicationContext.xml");
	 UserMapper userMapper = context.getBean(UserMapper.class);
	 User user = new User(7, "寒冰", "弗雷尔卓德");
	 userMapper.insert(user);
	}
}
